<?php

namespace App\Tests;
use App\Entity\Actualite;
use App\Entity\Commentaire;
use App\Entity\Produit;
use DateTime;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class CommentaireUnitTest extends TestCase
{
    public function testIsTrue()
    {
      $commentaire = new Commentaire();
      $date=new DateTimeImmutable();
      $actualite=new Actualite();
      $produit= new Produit();

      $commentaire-> setAuteur('auteur')
              -> setEmail('email@test.com')
              -> setCreatedAt($date)
              -> setContenu('contenu')
              -> setProduit($produit)
              -> setActualite($actualite);

      $this->assertTrue($commentaire->getAuteur()==='auteur');
      $this->assertTrue($commentaire->getEmail()==='email@test.com');
      $this->assertTrue($commentaire->getCreatedAt()=== $date);
      $this->assertTrue($commentaire->getContenu()==='contenu');
      $this->assertTrue($commentaire->getProduit()===$produit);
      $this->assertTrue($commentaire->getActualite()=== $actualite );
      
    }
    public function testIsFalse()
    {
      $commentaire = new Commentaire();
      $date=new DateTimeImmutable();
      $actualite=new Actualite();
      $produit= new Produit();

      $commentaire-> setAuteur('auteur')
              -> setEmail('email@test.com')
              -> setCreatedAt($date)
              -> setContenu('contenu')
              -> setProduit($produit)
              -> setActualite($actualite);

      $this->assertFalse($commentaire->getAuteur()==='false');
      $this->assertFalse($commentaire->getEmail()==='falseemail@test.com');
      $this->assertFalse($commentaire->getCreatedAt()=== new $date());
      $this->assertFalse($commentaire->getContenu()==='false');
      $this->assertFalse($commentaire->getProduit()=== new $produit());
      $this->assertFalse($commentaire->getActualite()=== new $actualite());
      
    }
    public function testIsEmpty()
    {
      $commentaire = new commentaire();    
           
      $this->assertEmpty($commentaire->getAuteur());
      $this->assertEmpty($commentaire->getEmail());
      $this->assertEmpty($commentaire->getCreatedAt());
      $this->assertEmpty($commentaire->getContenu());
      $this->assertEmpty($commentaire->getProduit());
      $this->assertEmpty($commentaire->getActualite());
      
    }
}
