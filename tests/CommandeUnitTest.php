<?php

namespace App\Tests;

use App\Entity\Commande;
use App\Entity\User;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class CommandeUnitTest extends TestCase
{
    public function testIsTrue()
    {
      $commande = new Commande();
      $datetime = new DateTimeImmutable();

      $commande -> setCreatedAt($datetime)
                -> setNombreArticle(4)
                -> setTotal(4.3);           
           
      $this->assertTrue($commande->getCreatedAt()===$datetime);
      $this->assertTrue($commande->getNombreArticle()=== 4);
      $this->assertTrue($commande->getTotal()=== 4.3);
      
       // $this->assertTrue(true);
    }
    public function testIsFalse()
    {
      $commande = new Commande();
      $datetime = new DateTimeImmutable();

      $commande -> setCreatedAt($datetime)
                -> setNombreArticle(4)
                -> setTotal(4.3);
           
      
      $this->assertFalse($commande->getCreatedAt()=== new $datetime());
      $this->assertFalse($commande->getNombreArticle()===3);
      $this->assertFalse($commande->getTotal()=== 4.1);
      
       // $this->assertTrue(true);
    }
    public function testIsEmpty()
    {
      $commande = new Commande();    
  
      $this->assertEmpty($commande->getCreatedAt());
      $this->assertEmpty($commande->getNombreArticle());
      $this->assertEmpty($commande->getTotal());

      
    }
}
