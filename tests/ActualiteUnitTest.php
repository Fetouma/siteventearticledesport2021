<?php

namespace App\Tests;

use App\Entity\Actualite;
use App\Entity\User;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class ActualiteUnitTest extends TestCase
{
    public function testIsTrue()
    {
      $actualite = new Actualite();
      $datetime = new DateTimeImmutable();

      $actualite-> setTitre('titre')
                -> setCreatedAt($datetime)
                -> setContenu('contenu')
                -> setSlug('slug');           
           
      $this->assertTrue($actualite->getTitre()==='titre');
      $this->assertTrue($actualite->getCreatedAt()===$datetime);
      $this->assertTrue($actualite->getContenu()==='contenu');
      $this->assertTrue($actualite->getSlug()==='slug');
      
       // $this->assertTrue(true);
    }
    public function testIsFalse()
    {
      $actualite = new Actualite();
      $datetime = new DateTimeImmutable();

      $actualite-> setTitre('titre')
                -> setCreatedAt($datetime)
                -> setContenu('contenu')
                -> setSlug('slug');
           
           
      $this->assertFalse($actualite->getTitre()==='false');
      $this->assertFalse($actualite->getCreatedAt()=== new $datetime());
      $this->assertFalse($actualite->getContenu()==='false');
      $this->assertFalse($actualite->getSlug()==='false');
      
       // $this->assertTrue(true);
    }
    public function testIsEmpty()
    {
      $actualite = new actualite();    
           
      $this->assertEmpty($actualite->getTitre());
      $this->assertEmpty($actualite->getCreatedAt());
      $this->assertEmpty($actualite->getContenu());
      $this->assertEmpty($actualite->getSlug());

      
    }
}
